const path = require("path");

module.exports = {
    entry: "./src/ReduxApplication.tsx",
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, "build")
    },

    mode: process.env.ENVIRONMENT || "development",

    devtool: "source-map",

    devServer: {
        contentBase: "./",
        historyApiFallback: true,
        port: 3001
    },

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json", ".scss"]
    },

    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader"
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            }
        ]
    },

    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    }
};
