import {BasicAction} from "./BasicAction";
import {PaymentOptionsListState} from "../components/cart/payment-options/_store/PaymentOptionsListState";

export class PaymentOptionsAction extends BasicAction {
    payload: PaymentOptionsListState;
}
