import {Action} from "redux";

export class BasicAction implements Action {
    type: string;
    payload: any;

    constructor(type: string, payload: any) {
        this.type = type;
        this.payload = payload;

        return {
            type: this.type,
            payload: this.payload
        }
    }
}
