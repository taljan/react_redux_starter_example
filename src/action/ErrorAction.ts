import {BasicAction} from "./BasicAction";
import {ErrorResponse} from "../model/error/ErrorData";

export class ErrorAction extends BasicAction {
    payload: ErrorResponse;
}
