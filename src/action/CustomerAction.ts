import {BasicAction} from "./BasicAction";
import {CustomerData} from "../model/customer/CustomerData";

export class CustomerAction extends BasicAction {
    payload: CustomerData;
}
