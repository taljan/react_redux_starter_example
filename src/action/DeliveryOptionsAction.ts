import {BasicAction} from "./BasicAction";
import {DeliveryOptionsListState} from "../components/cart/delivery-options/_store/DeliveryOptionsListState";

export class DeliveryOptionsAction extends BasicAction {
    payload: DeliveryOptionsListState;
}
