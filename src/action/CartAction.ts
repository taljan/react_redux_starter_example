import {BasicAction} from "./BasicAction";
import {CartData} from "../model/cart/CartData";

export class CartAction extends BasicAction {
    payload: CartData;
}
