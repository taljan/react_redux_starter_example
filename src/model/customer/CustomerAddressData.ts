export interface CustomerAddressData {
    street: string;
    zip: string;
    number: string;
    city: string;
    countryCode: string;
    addon: string;
    firstName: string;
    lastName: string;
    gender: string;

    title?: string;
    email?: string;
    addressId?: number;
}
