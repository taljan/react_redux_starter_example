import {CustomerAddressData} from "./CustomerAddressData";

export interface CustomerData {
    email: string;
    birthday?: string;
    walkInCustomer: boolean;
    address: CustomerAddressData;
}
