import {KeyValuePair} from "../../filter/KeyValuePair";
import {CustomerData} from "./CustomerData";

export class Customer {
    public static build = (data: Array<KeyValuePair>): CustomerData => {
        let email: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerEmail";
        });

        let gender: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerGender";
        });

        let firstName: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerFirstname";
        });

        let lastName: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerLastname";
        });

        let streetNumber: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerNumber";
        });

        let street: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerStreet";
        });

        let zip: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerZip";
        });

        let city: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerCity";
        });

        let birthday: KeyValuePair = data.find((paramPair: KeyValuePair) => {
            return paramPair.key === "gspCustomerBirthday";
        });

        return {
            email: email ? email.value : null,
            walkInCustomer: false,
            birthday: birthday ? birthday.value : "01.01.1970",
            address: {
                street: street ? street.value : null,
                zip: zip ? zip.value : null,
                number: streetNumber ? streetNumber.value : null,
                city: city ? city.value : null,
                countryCode: "de",
                addon: "",
                firstName: firstName ? firstName.value : null,
                lastName: lastName ? lastName.value : null,
                gender: gender ? gender.value : null,
            }
        }
    };
}
