export interface CustomerQueryData {
    gspCompanyNumber: string;
    gspVstNumber: string;
    gspStoreEmail: string;
    gspCustomerEmail: string;
    gspCustomerPhone: string;
    gspCustomerGender: string;
    gspCustomerTitle: string;
    gspCustomerFirstname: string;
    gspCustomerLastname: string;
    gspCustomerStreet: string;
    gspCustomerNumber: string;
    gspCustomerAddressaddon: string;
    gspCustomerZip: string;
    gspCustomerCity: string;
    gspEmployeeName: string;
    gspEmployeeNumber: string;
    gspCustomerDcNumber: string;
}
