export interface ErrorData {
    code: number;
    message: string;
    field: string;
}

export interface ErrorResponse {
    message: string;
    errors: Array<ErrorData>
}
