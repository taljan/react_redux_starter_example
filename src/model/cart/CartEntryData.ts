import {ProductPriceData} from "./ProductPriceData";
import {ProductData} from "./ProductData";
import {VoucherCodeData} from "./VoucherCodeData";

export interface CartEntryData {
    product: ProductData;
    price: ProductPriceData;
    quantity: number,
    entryNumber: number,
    vouchers: Array<VoucherCodeData>
}

