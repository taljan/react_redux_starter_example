export interface VoucherCodeData {
    douglasVoucherCode: string;
    value: number;
    code: string
}
