import {ProductData} from "./ProductData";
import {ProductPriceData} from "./ProductPriceData";


export interface BasketItemData {
    product?: ProductData,
    price?: ProductPriceData,
    quantity: number,
    entryNumber?: number
}
