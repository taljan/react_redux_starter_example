export interface PriceData {
    basePrice: number;
    loweredPrice: number;
    totalPrice: number;
    subPrice: number;
    shipping: number;
    currency: string;
}
