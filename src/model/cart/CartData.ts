import {CustomerData} from "../customer/CustomerData";
import {CartEntryData} from "./CartEntryData";
import {PriceData} from "./PriceData";
import {VoucherCodeData} from "./VoucherCodeData";
import {CustomerAddressData} from "../customer/CustomerAddressData";
import {DeliveryOptionData} from "./DeliveryOptionData";
import {PaymentOptionData} from "./PaymentOptionData";

export interface CartData  {
    cartId: string;
    entries: Array<CartEntryData>;
    price: PriceData;
    customer: CustomerData;
    vouchers: Array<VoucherCodeData>;
    deliveryMode: DeliveryOptionData;
    billingAddress: CustomerAddressData;
    deliveryAddress: CustomerAddressData;
    paymentType: PaymentOptionData;
}
