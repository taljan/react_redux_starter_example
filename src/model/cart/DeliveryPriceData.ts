export interface DeliveryPriceData {
    value: number;
    currencyIso: string;
    net: boolean;
}
