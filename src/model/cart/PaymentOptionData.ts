export interface PaymentOptionData {
    key: string;
    localizedName: string;
    preselected: boolean;
}
