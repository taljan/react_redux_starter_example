import {DeliveryPriceData} from "./DeliveryPriceData";

export interface DeliveryOptionData {
    zoneDeliveryModeCode: string;
    name: string;
    price: DeliveryPriceData;
    preselected: boolean;
}
