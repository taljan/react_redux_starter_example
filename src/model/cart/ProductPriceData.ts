import {PriceData} from "./PriceData";

export interface ProductPriceData extends PriceData{
    loweredUnit: string;
    baseUnitSize: string;
}
