export interface ProductData {
    productId: string;
    designerCategoryName: string;
    totalAvailable: number;
    image: string;
    masterProductName: string;
    classificationClassAsString: string;
    contentUnit: string;
    lineCategoryName: string;
}
