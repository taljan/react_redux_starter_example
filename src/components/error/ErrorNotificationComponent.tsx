import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../store/Store";
import {ErrorData} from "../../model/error/ErrorData";
import {connect} from "react-redux";
import {ErrorState} from "./_store/ErrorState";

interface Subscriptions {
    errorState?: ErrorState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        errorState: state.errorState
    }
};

export class ErrorNotificationComponent extends Component<Subscriptions> {
    render() {
        if (this.props.errorState.errors.length >= 1) {
            window.scrollTo(0,0);
        }
        return (
            <div id={`errorMessages`} className={`row`}>
                <div className={`col-12`}>
                    {this.props.errorState.errors.map((error: ErrorData) =>
                        <div key={error.field || error.code} className={`alert alert-danger`} role={`alert`}>
                            <strong>Fehler!</strong><br/><span>{error.message}</span>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(ErrorNotificationComponent);
