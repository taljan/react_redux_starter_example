import {ErrorState} from "./ErrorState";
import {ErrorAction} from "../../../action/ErrorAction";
import {ErrorReducer} from "./ErrorReducer";

export const errorReducer = new ErrorReducer(ErrorReducer.InitialState)
    .subscribe(
        ErrorReducer.Actions.ReceiveError,
        (state: ErrorState, action: ErrorAction) => {
            return {
                ...action.payload,
            }
        })
    .build();
