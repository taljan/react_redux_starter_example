import {AbstractStore} from "../../../store/Reducer";
import {ErrorState} from "./ErrorState";

const InitialState: ErrorState = {
    message: "",
    errors: []
};

export class ErrorReducer extends AbstractStore<ErrorState> {
    public static Actions = {
        ReceiveError: "ReceiveError"
    };

    public static InitialState: ErrorState = InitialState;
}

