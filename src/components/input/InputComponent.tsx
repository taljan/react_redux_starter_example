import * as React from "react";

interface IProperties {
    type: string,
    id: string,
    placeholder?: string,
    required?: boolean,
    label: string,
    invalidityText?: string,
    defaultValue?: string,
    value?: string
}

interface IEventDispatchers {
    onChange?(e: React.ChangeEvent<HTMLInputElement>): void,

    onKeyUp?(e: React.KeyboardEvent<HTMLInputElement>): void
}

type CombinedProperties = IProperties & IEventDispatchers;

export class InputComponent extends React.Component<CombinedProperties> {

    public static Type = {
        Text: "text",
        Email: "email",
        Number: "number"
    };

    render() {
        return (
            <div className={"form-group"}>
                <label htmlFor={this.props.id}>{this.props.label}</label>
                <input type={this.props.type} className={"form-control"} id={this.props.id}
                       placeholder={this.props.placeholder || this.props.label} required={this.props.required || false}
                       onChange={this.props.onChange}
                       onKeyUp={this.props.onKeyUp}
                       defaultValue={this.props.defaultValue}
                       value={this.props.value}
                       onBlur={(e: React.FocusEvent<HTMLInputElement>) => {
                           if (!e.currentTarget.checkValidity()) {
                               e.currentTarget.classList.add("is-invalid");
                           } else {
                               e.currentTarget.classList.remove("is-invalid");
                           }
                       }}/>
                <div className={"invalid-feedback"}>
                    {this.props.invalidityText}
                </div>
            </div>
        );
    }
}
