import * as React from "react";

export interface ISelectOption {
    value: string;
    label: string;
}

interface IProperties {
    id: string,
    required?: boolean,
    label: string,
    options: Array<ISelectOption>,
    defaultOption: ISelectOption,
    defaultValue?: string,
    invalidityText?: string
}

interface IEventDispatchers {
    onChange?(e: React.ChangeEvent<Element>): void
}

type CombinedProperties = IProperties & IEventDispatchers;

export class SelectComponent extends React.Component<CombinedProperties> {

    render() {
        return (
            <div className={"form-group"}>
                <label htmlFor={this.props.id}>{this.props.label}</label><br/>
                <select id={this.props.id} required={this.props.required || false}
                        defaultValue={this.props.defaultValue}
                        onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                            if (!e.currentTarget.checkValidity()) {
                                e.currentTarget.classList.add("is-invalid");
                            } else {
                                e.currentTarget.classList.remove("is-invalid");
                            }
                            this.props.onChange(e);
                        }}>
                    <option value={this.props.defaultOption.value}>{this.props.defaultOption.label}</option>
                    {this.props.options.map((option: ISelectOption) =>
                        <option key={option.value} value={option.value}>{option.label}</option>
                    )}
                </select>
            </div>
        );
    }
}
