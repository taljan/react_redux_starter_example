import * as React from "react";
import {Component} from "react";
import {CustomerAddressData} from "../../../model/customer/CustomerAddressData";
import {GenderFiler} from "../../../filter/GenderFilter";
import {CountryCodeFilter} from "../../../filter/CountryCodeFilter";
import {LoadingIndicator} from "../../loading-indicator/LoadingIndicator";
import {AddressFormComponent} from "./AddressFormComponent";

interface Properties {
    title: string;
    address: CustomerAddressData;
    id: string;
    editable?: boolean;
    altText?: string;
}

interface Dispatchers {
    onEditAddress?: (address: CustomerAddressData) => void
}

type ComponentProperties = Properties & Dispatchers;

export class AddressPanelComponent extends Component<ComponentProperties> {
    render() {
        return (
            <div className={`col-6`}>
                <div id={this.props.id} className={`panel address`}>

                    <h5>{this.props.title}:</h5>
                    {this.props.address &&
                    <div>
                        <span className={`gender`}>{GenderFiler(this.props.address.gender)}</span>&nbsp;
                        <span className={`firstName`}>{this.props.address.firstName}</span>&nbsp;
                        <span className={`lastName`}>{this.props.address.firstName}</span><br/>
                        <span className={`street`}>{this.props.address.street}</span>&nbsp;
                        <span className={`number`}>{this.props.address.number}</span><br/>
                        {this.props.address.addon &&
                        <span className={`addressAddon`}>{this.props.address.addon}<br/></span>}
                        <span className={`zip`}>{this.props.address.zip}</span>&nbsp;
                        <span className={`city`}>{this.props.address.city}</span><br/><br/>
                        <span className={`country`}>{CountryCodeFilter(this.props.address.countryCode)}</span>
                        <AddressFormComponent address={this.props.address} type={this.props.id}/>
                    </div>
                    ||
                    <LoadingIndicator/>
                    }
                    {this.props.editable &&
                    <div className={`pull-bottom`}>
                        <a href={`#`}>{`${this.props.title} bearbeiten`}</a>
                    </div>
                    }
                </div>
            </div>
        )
    }
}
