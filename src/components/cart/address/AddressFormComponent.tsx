import * as React from "react";
import {Component} from "react";
import {CustomerAddressData} from "../../../model/customer/CustomerAddressData";
import {AddressService} from "../../../api/AddressService";
import {AxiosResponse} from "axios";
import {CartService} from "../../../api/CartService";
import {QueryService} from "../../../api/QueryService";
import {CountryCodeFilter} from "../../../filter/CountryCodeFilter";

interface Properties {
    address: CustomerAddressData,
    type: string
}

interface Dispatchers {
    onEditAddress?: (address: CustomerAddressData) => void
}

export interface AddressFormState {
    formAddress: CustomerAddressData,
    suggestedAddress: CustomerAddressData
}

type ComponentProperties = Properties & Dispatchers;

export class AddressFormComponent extends Component<ComponentProperties> {
    handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        let data: FormData = new FormData(e.currentTarget);

        let values: CustomerAddressData = {
            street: "",
            zip: "",
            number: "",
            city: "",
            countryCode: "",
            addon: "",
            firstName: "",
            lastName: "",
            gender: ""
        };

        data.forEach((value: FormDataEntryValue, key: string) => {
            values = {
                ...values,
                [key]: value
            }
        });

        this.setState({
            ...this.state,
            formAddress: values
        }, () => {
            AddressService.checkAddress(this.state.formAddress).then((response: AxiosResponse) => {
                if (this.addressEquals(this.state.formAddress, response.data)) {
                    if (this.props.type === "billingAddress") {
                        CartService.setBillingAddress(response.data).then(() => {
                            CartService.fetchCart(QueryService.getCartId());
                        });
                    } else {
                        CartService.setDeliveryAddress(response.data).then(() => {
                            CartService.fetchCart(QueryService.getCartId());
                        });
                    }
                } else {
                    this.setState({
                        ...this.state,
                        suggestedAddress: response.data
                    })
                }
            });
        });
    };

    applySuggestion = (e: React.MouseEvent<HTMLButtonElement>) => {
        CartService.setDeliveryAddress(this.state.suggestedAddress).then(() => {
            CartService.fetchCart(QueryService.getCartId());
        });
    };

    addressEquals = (a: CustomerAddressData, b: CustomerAddressData): boolean => {
        if (a.zip !== b.zip) return false;
        if (a.number !== b.number) return false;
        if (a.city !== b.city) return false;
        if (a.street !== b.street) return false;
        return (a.countryCode === b.countryCode);
    };

    constructor(props: ComponentProperties) {
        super(props);
        this.state = {
            formAddress: this.props.address,
            suggestedAddress: null
        };
    }

    state: AddressFormState;

    render() {
        return (
            <div className={`address-form`}>
                <form onSubmit={this.handleFormSubmit} noValidate={true}>
                    <select name={`gender`} defaultValue={this.state.formAddress.gender}>
                        <option value={`f`}>Frau</option>
                        <option value={`m`}>Herr</option>
                    </select>
                    <input name={`firstName`} defaultValue={this.state.formAddress.firstName} type={`text`}/>
                    <input name={`lastName`} defaultValue={this.state.formAddress.lastName} type={`text`}/>
                    <input name={`street`} defaultValue={this.state.formAddress.street} type={`text`}/>
                    <input name={`zip`} defaultValue={this.state.formAddress.zip} type={`number`}/>
                    <input name={`number`} defaultValue={this.state.formAddress.number} type={`number`}/>
                    <input name={`city`} defaultValue={this.state.formAddress.city} type={`text`}/>
                    <input name={`addon`} defaultValue={this.state.formAddress.addon} type={`text`}/>
                    <input name={`countryCode`} type={`hidden`} defaultValue={this.state.formAddress.countryCode}/>
                    <button type={`submit`} className={`btn btn-primary`}>Speichern</button>
                </form>
                {this.state.suggestedAddress &&
                <div>
                    <p>Vorgeschlagene Addresse:</p>
                    <p>
                        {this.state.suggestedAddress.street}&nbsp;{this.state.suggestedAddress.number}<br/>
                        {this.state.suggestedAddress.zip}&nbsp;{this.state.suggestedAddress.city}<br/>
                        {CountryCodeFilter(this.state.suggestedAddress.countryCode)}<br/>
                        <button type={`button`} className={`btn btn-primary`} onClick={this.applySuggestion}>Vorschlag
                            übernehmen
                        </button>
                    </p>
                </div>

                }
            </div>
        )
    }
}
