import {CartReducer} from "../../_store/CartReducer";
import {AddressState} from "./AddressState";
import {CartAction} from "../../../../action/CartAction";
import {AddressReducer} from "./AddressReducer";

export const addressReducer = new AddressReducer(AddressReducer.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: AddressState, action: CartAction) => {
            /**
             * TODO: Workaround Fix gender coming from the backend
             */
            action.payload.billingAddress.gender = action.payload.billingAddress.gender === "FEMALE" ? "f" : "m";
            action.payload.deliveryAddress.gender = action.payload.deliveryAddress.gender === "FEMALE" ? "f" : "m";
            return {
                ...state,
                billingAddress: action.payload.billingAddress,
                deliveryAddress: action.payload.deliveryAddress
            }
        }
    )
    .build();
