import {AbstractStore} from "../../../../store/Reducer";
import {AddressState} from "./AddressState";

const InitialState: AddressState = {
    billingAddress: null,
    deliveryAddress: null
};

export class AddressReducer extends AbstractStore<AddressState> {
    public static InitialState: AddressState = InitialState;
}

