import {CustomerAddressData} from "../../../../model/customer/CustomerAddressData";

export interface AddressState {
    billingAddress: CustomerAddressData;
    deliveryAddress: CustomerAddressData;
}
