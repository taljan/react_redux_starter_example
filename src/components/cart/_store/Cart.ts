import {CartState} from "./CartState";
import {CartAction} from "../../../action/CartAction";
import {CartReducer} from "./CartReducer";

export const cartReducer = new CartReducer(CartReducer.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: CartState, action: CartAction) => {
            return {
                ...action.payload,
            }
        })
    .build();
