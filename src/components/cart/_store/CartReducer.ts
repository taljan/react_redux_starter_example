import {AbstractStore} from "../../../store/Reducer";
import {CartState} from "./CartState";

const InitialState: CartState = {
    cartId: null,
    entries: null,
    price: null,
    customer: null,
    vouchers: null,
    deliveryMode: null,
    deliveryAddress: null,
    billingAddress: null,
    paymentType: null
};

export class CartReducer extends AbstractStore<CartState> {
    public static Actions = {
        ReceiveCartData: "ReceiveCartData"
    };

    public static InitialState: CartState = InitialState;
}

