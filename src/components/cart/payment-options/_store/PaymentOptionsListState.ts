import {PaymentOptionData} from "../../../../model/cart/PaymentOptionData";

export interface PaymentOptionsListState extends Array<PaymentOptionData> {
}
