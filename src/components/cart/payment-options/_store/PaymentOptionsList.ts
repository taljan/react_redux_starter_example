import {PaymentOptionsListState} from "./PaymentOptionsListState";
import {PaymentOptionsAction} from "../../../../action/PaymentOptionsAction";
import {PaymentOptionsListReducer} from "./PaymentOptionsListReducer";

export const paymentOptionsListReducer = new PaymentOptionsListReducer(PaymentOptionsListReducer.InitialState)
    .subscribe(
        PaymentOptionsListReducer.Actions.ReceiveAvailablePaymentOptions,
        (state: PaymentOptionsListState, action: PaymentOptionsAction) => {
            return action.payload;
        }
    )
    .build();
