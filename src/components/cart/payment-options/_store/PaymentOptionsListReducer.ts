import {AbstractStore} from "../../../../store/Reducer";
import {PaymentOptionsListState} from "./PaymentOptionsListState";

const InitialState: PaymentOptionsListState = [];

export class PaymentOptionsListReducer extends AbstractStore<PaymentOptionsListState> {
    public static Actions = {
        ReceiveAvailablePaymentOptions: "ReceiveAvailablePaymentOptions"
    };

    public static InitialState: PaymentOptionsListState = InitialState;
}

