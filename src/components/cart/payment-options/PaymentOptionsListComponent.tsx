import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {connect} from "react-redux";
import {PaymentOptionData} from "../../../model/cart/PaymentOptionData";
import {PaymentOptionsListState} from "./_store/PaymentOptionsListState";
import {LoadingIndicator} from "../../loading-indicator/LoadingIndicator";
import {CartService} from "../../../api/CartService";

interface Subscriptions {
    paymentOptionsState?: PaymentOptionsListState,
    selectedPaymentOption?: PaymentOptionData
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        paymentOptionsState: state.paymentOptionsState,
        selectedPaymentOption: state.cartDataState.paymentType
    }
};

export class PaymentOptionsListComponent extends Component<Subscriptions> {
    render() {
        return (
            <div className={`col-6`} id={`paymentOptions`}>
                <div className={`panel`}>
                    <h5>Zahlart</h5>
                    {this.props.paymentOptionsState.length >= 1 && <form>
                        <h6 className={`h6`}>DHL</h6>
                        {this.props.paymentOptionsState.map((paymentOption: PaymentOptionData) =>
                            <div className={`form-check`} key={paymentOption.key}>
                                <input className={`form-check-input`} type={`radio`} name={`delivery-options`}
                                       id={`delivery-option-${paymentOption.key}`}
                                       value={paymentOption.key}
                                       defaultChecked={this.props.selectedPaymentOption.key === paymentOption.key}
                                       onChange={(e: React.ChangeEvent) => {
                                           CartService.setPaymentOption(paymentOption.key);
                                       }} />
                                <label className={`form-check-label`}
                                       htmlFor={`delivery-option-${paymentOption.key}`}>
                                    {paymentOption.localizedName}
                                </label>
                            </div>
                        )}
                    </form>
                    ||
                    <LoadingIndicator/>
                    }
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(PaymentOptionsListComponent);
