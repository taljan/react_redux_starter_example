import * as React from "react";
import {Component} from "react";
import {withRouter} from "react-router";
import {CustomerDataDisplayComponent} from "./CustomerDataDisplayComponent";
import {ApplicationStore} from "../../../store/Store";

import {CustomerService} from "../../../api/CustomerService";
import {connect} from "react-redux";
import {CartService} from "../../../api/CartService";
import PaymentOptionsListComponent from "../payment-options/PaymentOptionsListComponent";
import DeliveryOptionsListComponent from "../delivery-options/DeliveryOptionsListComponent";
import {AddressState} from "../address/_store/AddressState";
import {CustomerState} from "./_store/CustomerState";
import {QueryService} from "../../../api/QueryService";

interface Properties {
    location: any,
    match: any,
    history: any
}

interface Subscriptions {
    customerState?: CustomerState,
    addressState?: AddressState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        customerState: state.customerState,
        addressState: state.addressState
    }
};

type CustomerProperties = Properties & Subscriptions;

export class CustomerComponent extends Component<CustomerProperties> {

    constructor(properties: CustomerProperties) {
        super(properties);
        CustomerService.createCustomer().then(() => {
            CartService.fetchAvailablePaymentOptions(QueryService.getCartId());
            CartService.fetchAvailableDeliveryOptions(QueryService.getCartId());
        });
    }


    render() {

        return (
            <div id={`customer`}>
                {this.props.customerState.email &&
                <div>
                    <CustomerDataDisplayComponent email={this.props.customerState.email}
                                                  walkInCustomer={this.props.customerState.walkInCustomer}
                                                  address={this.props.customerState.address}
                                                  addressState={this.props.addressState}/>
                    <div className={`row`}>
                        <PaymentOptionsListComponent/>
                        <DeliveryOptionsListComponent/>
                    </div>
                </div>
                }

                {!this.props.customerState.email && <p>There should be a form</p>}
            </div>
        )
    }
}

export default connect<Subscriptions>(mapStateToProps)(withRouter(CustomerComponent));
