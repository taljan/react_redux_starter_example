import {AbstractStore} from "../../../../store/Reducer";
import {CustomerState} from "./CustomerState";

const InitialState: CustomerState = {
    email: "",
    birthday: "",
    walkInCustomer: false,
    address: null
};

export class CustomerReducer extends AbstractStore<CustomerState> {
    public static Actions = {
        ResolveCustomerQueryData: "ResolveCustomerQueryData",
        ReceiveCustomerData: "ReceiveCustomerData"
    };

    public static InitialState: CustomerState = InitialState;
}

