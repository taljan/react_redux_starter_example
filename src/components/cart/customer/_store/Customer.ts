import {CustomerState} from "./CustomerState";
import {CustomerAction} from "../../../../action/CustomerAction";
import {CartReducer} from "../../_store/CartReducer";
import {CartAction} from "../../../../action/CartAction";
import {CustomerReducer} from "./CustomerReducer";

export const customerReducer = new CustomerReducer(CustomerReducer.InitialState)
    .subscribe(
        CustomerReducer.Actions.ResolveCustomerQueryData,
        (state: CustomerState, action: CustomerAction) => {
            return {
                ...action.payload,
            }
        })
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: CustomerState, action: CartAction) => {
            return {
                ...action.payload.customer,
            }
        })
    .build();
