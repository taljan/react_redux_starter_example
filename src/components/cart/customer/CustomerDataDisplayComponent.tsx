import * as React from "react";
import {Component} from "react";
import {CustomerData} from "../../../model/customer/CustomerData";
import {AddressState} from "../address/_store/AddressState";
import {AddressPanelComponent} from "../address/AddressPanelComponent";
import {CustomerAddressData} from "../../../model/customer/CustomerAddressData";

interface Properties extends CustomerData {
    addressState: AddressState
}

export class CustomerDataDisplayComponent extends Component<Properties> {
    render() {
        return (
            <div id={`customer`}>
                {this.props.email &&
                <div>
                    <div className={`row`}>
                        <AddressPanelComponent id={`billingAddress`} title={"Rechnungsadresse"}
                                               editable={true}
                                               address={this.props.addressState.billingAddress}
                                               altText={`Bitte geben Sie eine Rechnungsadresse an`}
                                               onEditAddress={(newAddress: CustomerAddressData) => {
                                          console.log("New Billing Address", newAddress);
                                      }}/>

                        <AddressPanelComponent id={`deliveryAddress`} title={"Lieferadresse"}
                                               address={this.props.addressState.deliveryAddress}
                                               editable={true}
                                               altText={`Wie Rechnungsadresse`}
                                               onEditAddress={(newAddress: CustomerAddressData) => {
                                          console.log("New Delivery Address", newAddress);
                                      }}/>

                    </div>
                    <div className={`row`}>

                    </div>
                </div>
                }
            </div>
        )
    }
}
