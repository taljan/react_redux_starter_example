import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../store/Store";
import {connect} from "react-redux";
import {ApplicationState} from "../../store/application/ApplicationState";
import BasketList from "./basket-list/BasketListComponent";
import PricePanelComponent from "./price/PricePanelComponent";
import EmployeeVoucherComponent from "./voucher/EmployeeVoucherComponent";
import NewCustomerVoucherComponent from "./voucher/NewCustomerVoucherComponent";
import VoucherComponent from "./voucher/VoucherComponent";

interface Subscriptions {
    applicationState?: ApplicationState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        applicationState: state.applicationState
    }
};

export class ShoppingCart extends Component<Subscriptions> {
    render() {
        return (
            <div className={`row`} id={`shoppingCart`}>
                {this.props.applicationState.hasCart &&
                <div>
                    <div className={`col col-12`}>
                        <BasketList/>
                    </div>
                    <EmployeeVoucherComponent/>
                    <NewCustomerVoucherComponent/>
                    <VoucherComponent/>
                    <PricePanelComponent/>
                    <p><br/>Ich akzeptiere die AGB und habe die Widerrufsbelehrung sowie die Datenschutzbestimmungen in
                        ihrer aktuellen Fassung zur Kenntniss genommen.</p>
                </div>
                }
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(ShoppingCart);
