import * as React from "react";
import {Component} from "react";
import {PriceFilter} from "../../../filter/PriceFilter";
import {CurrencyFilter} from "../../../filter/CurrencyFilter";
import {ApplicationStore} from "../../../store/Store";
import {PriceData} from "../../../model/cart/PriceData";
import {connect} from "react-redux";
import {VoucherState} from "../voucher/_store/VoucherState";
import {VoucherCodeData} from "../../../model/cart/VoucherCodeData";

interface Subscriptions {
    priceData?: PriceData,
    voucherData?: VoucherState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        priceData: state.cartDataState.price,
        voucherData: state.voucherState
    }
};

export class PricePanelComponent extends Component<Subscriptions> {
    render() {
        let discount: number = 0;
        this.props.voucherData.forEach((voucherData: VoucherCodeData) => {
            discount += voucherData.value;
        });
        return (
            <div id={`price`} className={`row`}>
                <div className={`col-12`}>
                    <div className={"panel"} style={{clear: "both", overflow: "hidden"}}>
                        <table className={"float-right"} style={{margin: "10px 30px 5px 0"}}>
                            <tbody>
                            <tr>
                                <td style={{minWidth: "250px"}}><h6>Zwischensumme:</h6></td>
                                <td>
                                    <h6 className={"float-right"}>{PriceFilter(this.props.priceData.subPrice)}{CurrencyFilter(this.props.priceData.currency)}</h6>
                                </td>
                            </tr>
                            <tr>
                                <td><p>Versandkosten:</p></td>
                                <td>
                                    <p className={"float-right"}>{PriceFilter(this.props.priceData.shipping)}{CurrencyFilter(this.props.priceData.currency)}</p>
                                </td>
                            </tr>
                            <tr>
                                <td><p>abzgl. Rabatt:</p></td>
                                <td>
                                    <p className={"float-right"}>{PriceFilter(discount)}{CurrencyFilter(this.props.priceData.currency)}</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6>Gesamtsumme:<br/>
                                        <small>inkl. MwSt.</small>
                                    </h6>
                                </td>
                                <td>
                                    <h6 className={"float-right"}>{PriceFilter(this.props.priceData.totalPrice)}{CurrencyFilter(this.props.priceData.currency)}</h6>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(PricePanelComponent);

