import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {VoucherState} from "./_store/VoucherState";
import {VoucherCodeData} from "../../../model/cart/VoucherCodeData";
import {CartService} from "../../../api/CartService";
import {connect} from "react-redux";
import {VOUCHER_EMPLOYEE, VOUCHER_NEW_CUSTOMER} from "../../../api/Configuration";

interface Subscriptions {
    voucherState?: VoucherState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        voucherState: state.voucherState
    }
};

interface State {
    voucherCode: string;
}

type ComponentProperties = Subscriptions;

export class VoucherComponent extends Component<ComponentProperties> {
    constructor(props: ComponentProperties) {
        super(props);
        this.state = {
            voucherCode: ""
        };
    }

    state: State;

    handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({...this.state, voucherCode: e.target.value});
    };

    render() {

        let voucherList: Array<VoucherCodeData> = this.props.voucherState;
        return (
            <div id={`voucher`} className={`row`}>
                <div className={`col-12`}>
                    <div className={`panel`}>
                        <h5>Rabattcodes</h5>
                        {voucherList.length >= 1 &&
                        <ul>
                            {voucherList.map((voucherData: VoucherCodeData) => {
                                if(voucherData.douglasVoucherCode !== VOUCHER_NEW_CUSTOMER && voucherData.douglasVoucherCode !== VOUCHER_EMPLOYEE)
                                    return <li key={voucherData.douglasVoucherCode}>{voucherData.code}</li>
                                }
                            )}
                        </ul>
                        }

                        <div className="input-group mb-3">
                            <input type={`text`} className={`form-control`} placeholder={`Rabattcode`}
                                   aria-label={`Rabattcode`} name={`voucherCode`} onChange={this.handleChange}/>
                            <div className={`input-group-append`}>
                                <button className={`btn btn-outline-primary`}
                                        onClick={(e: React.MouseEvent) => {
                                            CartService.addVoucher(this.state.voucherCode);
                                        }}
                                        type={`button`}>Hinzufügen
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(VoucherComponent);
