import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {connect} from "react-redux";
import {CartService} from "../../../api/CartService";
import {VOUCHER_EMPLOYEE} from "../../../api/Configuration";

interface Subscriptions {
    employeeVoucherState?: boolean
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        employeeVoucherState: state.employeeVoucherState
    }
};

export class EmployeeVoucherComponent extends Component<Subscriptions> {
    render() {
        let voucherIsActive: boolean = this.props.employeeVoucherState;
        return (
            <div id={`employeeVoucher`} className={`row`}>
                <div className={`col-12`}>
                    <div className={`panel`}>
                        <h5>Mitarbeiterrabatt{voucherIsActive && <small>&nbsp;(eingelöst)</small>}</h5>
                        <button className={`btn btn-primary`} disabled={voucherIsActive}
                                onClick={(e: React.MouseEvent) => {
                                    CartService.addVoucher(VOUCHER_EMPLOYEE);
                                }}>Hinzufügen
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(EmployeeVoucherComponent);
