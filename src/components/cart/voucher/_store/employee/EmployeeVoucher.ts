import {EmployeeVoucherReducer} from "./EmployeeVoucherReducer";
import {CartReducer} from "../../../_store/CartReducer";
import {CartAction} from "../../../../../action/CartAction";
import {VoucherCodeData} from "../../../../../model/cart/VoucherCodeData";
import {VOUCHER_EMPLOYEE} from "../../../../../api/Configuration";


export const employeeVoucherReducer = new EmployeeVoucherReducer(EmployeeVoucherReducer.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: boolean, action: CartAction) => {
            let compare: Array<VoucherCodeData> = action.payload.vouchers.filter((voucher: VoucherCodeData) => {
                    return voucher.douglasVoucherCode.toLowerCase() === VOUCHER_EMPLOYEE.toLowerCase();
                }
            );

            return compare.length >= 1;
        }
    )
    .build();
