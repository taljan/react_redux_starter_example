import {CartReducer} from "../../_store/CartReducer";
import {VoucherState} from "./VoucherState";
import {CartAction} from "../../../../action/CartAction";
import {VoucherReducer} from "./VoucherReducer";

export const voucherReducer = new VoucherReducer(VoucherReducer.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: VoucherState, action: CartAction) => {
            return action.payload.vouchers
        }
    )
    .build();
