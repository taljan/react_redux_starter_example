import {CartReducer} from "../../../_store/CartReducer";
import {CartAction} from "../../../../../action/CartAction";
import {VoucherCodeData} from "../../../../../model/cart/VoucherCodeData";
import {VOUCHER_NEW_CUSTOMER} from "../../../../../api/Configuration";
import {NewCustomerVoucherReducer} from "./NewCustomerVoucherReducer";


export const newCustomerVoucherReducer = new NewCustomerVoucherReducer(NewCustomerVoucherReducer.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: boolean, action: CartAction) => {
            let compare: Array<VoucherCodeData> = action.payload.vouchers.filter((voucher: VoucherCodeData) => {
                    return voucher.douglasVoucherCode.toLowerCase() === VOUCHER_NEW_CUSTOMER.toLowerCase();
                }
            );

            return compare.length >= 1;
        }
    )
    .build();
