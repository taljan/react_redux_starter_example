import {AbstractStore} from "../../../../store/Reducer";
import {VoucherState} from "./VoucherState";

const InitialState: VoucherState = [];

export class VoucherReducer extends AbstractStore<VoucherState> {
    public static InitialState: VoucherState = InitialState;
}

