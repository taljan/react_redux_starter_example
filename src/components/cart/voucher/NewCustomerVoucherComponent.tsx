import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {connect} from "react-redux";
import {CartService} from "../../../api/CartService";
import {VOUCHER_NEW_CUSTOMER} from "../../../api/Configuration";

interface Subscriptions {
    newCustomerVoucherState?: boolean
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        newCustomerVoucherState: state.newCustomerVoucherState
    }
};

export class NewCustomerVoucherComponent extends Component<Subscriptions> {
    render() {
        let voucherIsActive: boolean = this.props.newCustomerVoucherState;
        return (
            <div id={`newCustomerVoucher`} className={`row`}>
                <div className={`col-12`}>
                    <div className={`panel`}>
                        <h5>Neukundenrabatt{voucherIsActive && <small>&nbsp;(eingelöst)</small>}</h5>
                        <button className={`btn btn-primary`} disabled={voucherIsActive}
                                onClick={(e: React.MouseEvent) => {
                                    CartService.addVoucher(VOUCHER_NEW_CUSTOMER);
                                }}>Hinzufügen
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(NewCustomerVoucherComponent);
