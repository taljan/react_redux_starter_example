import {AbstractStore} from "../../../../store/Reducer";
import {DeliveryOptionsListState} from "./DeliveryOptionsListState";

const InitialState: DeliveryOptionsListState = {
    DHL: [],
    Hermes: []
};

export class DeliveryOptionsListReducer extends AbstractStore<DeliveryOptionsListState> {
    public static Actions = {
        ReceiveAvailableDeliveryOptions: "ReceiveAvailableDeliveryOptions"
    };
    public static InitialState: DeliveryOptionsListState = InitialState;
}

