import {DeliveryOptionData} from "../../../../model/cart/DeliveryOptionData";

export interface DeliveryOptionsListState {
    DHL: Array<DeliveryOptionData>;
    Hermes: Array<DeliveryOptionData>;
}
