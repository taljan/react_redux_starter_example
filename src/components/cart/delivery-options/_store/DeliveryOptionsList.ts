import {DeliveryOptionsListState} from "./DeliveryOptionsListState";
import {DeliveryOptionsAction} from "../../../../action/DeliveryOptionsAction";
import {DeliveryOptionsListReducer} from "./DeliveryOptionsListReducer";

export const deliveryOptionsListReducer = new DeliveryOptionsListReducer(DeliveryOptionsListReducer.InitialState)
    .subscribe(
        DeliveryOptionsListReducer.Actions.ReceiveAvailableDeliveryOptions,
        (state: DeliveryOptionsListState, action: DeliveryOptionsAction) => {
            return action.payload;
        }
    )
    .build();
