import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {connect} from "react-redux";

import {DeliveryOptionsListState} from "./_store/DeliveryOptionsListState";
import {DeliveryOptionData} from "../../../model/cart/DeliveryOptionData";
import {LoadingIndicator} from "../../loading-indicator/LoadingIndicator";
import {CartService} from "../../../api/CartService";
import {CurrencyFilter} from "../../../filter/CurrencyFilter";
import {PriceFilter} from "../../../filter/PriceFilter";

interface Subscriptions {
    deliveryOptionsState?: DeliveryOptionsListState,
    selectedDeliveryOption?: DeliveryOptionData
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        deliveryOptionsState: state.deliveryOptionsState,
        selectedDeliveryOption: state.cartDataState.deliveryMode
    }
};

export class DeliveryOptionsListComponent extends Component<Subscriptions> {
    render() {
        return (
            <div className={`col-6`} id={`deliveryOptions`}>
                {this.props.selectedDeliveryOption.zoneDeliveryModeCode && <div className={`panel`}>
                    <h5>Versandart</h5>
                    <form>
                        <h6 className={`h6`}>DHL</h6>
                        {this.props.deliveryOptionsState.DHL.length >= 1 && this.props.deliveryOptionsState.DHL.map((deliveryOption: DeliveryOptionData) =>
                            <div className={`form-check`} key={deliveryOption.zoneDeliveryModeCode}>
                                <input className={`form-check-input`} type={`radio`} name={`delivery-options`}
                                       id={`delivery-option-${deliveryOption.name}`}
                                       value={deliveryOption.name}
                                       defaultChecked={this.props.selectedDeliveryOption.name === deliveryOption.name}
                                       onChange={(e: React.ChangeEvent) => {
                                           CartService.setDeliveryOption(deliveryOption.zoneDeliveryModeCode);
                                       }}/>
                                <label className={`form-check-label`}
                                       htmlFor={`delivery-option-${deliveryOption.name}`}>
                                    {deliveryOption.name} ({`${PriceFilter(deliveryOption.price.value)} ${CurrencyFilter(deliveryOption.price.currencyIso)}`})
                                </label>
                            </div>
                        ) || <LoadingIndicator/>}
                        <h6 className={`h6`}>Hermes</h6>
                        {this.props.deliveryOptionsState.Hermes.length >= 1 && this.props.deliveryOptionsState.Hermes.map((deliveryOption: DeliveryOptionData) =>
                            <div className={`form-check`} key={deliveryOption.zoneDeliveryModeCode}>
                                <input className={`form-check-input`} type={`radio`} name={`delivery-options`}
                                       id={`delivery-option-${deliveryOption.name}`}
                                       value={deliveryOption.name}
                                       defaultChecked={this.props.selectedDeliveryOption.name === deliveryOption.name}
                                       onChange={(e: React.ChangeEvent) => {
                                           CartService.setDeliveryOption(deliveryOption.zoneDeliveryModeCode);
                                       }}/>
                                <label className={`form-check-label`}
                                       htmlFor={`delivery-option-${deliveryOption.name}`}>
                                    {deliveryOption.name} ({`${PriceFilter(deliveryOption.price.value)} ${CurrencyFilter(deliveryOption.price.currencyIso)}`})
                                </label>
                            </div>
                        ) || <LoadingIndicator/>}
                    </form>
                </div>}
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(DeliveryOptionsListComponent);
