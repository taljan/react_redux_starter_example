import {AbstractStore} from "../../../../store/Reducer";
import {BasketListState} from "./BasketListState";


const InitialState: BasketListState = [];

export class BasketListStateStore extends AbstractStore<BasketListState> {

    public static InitialState: BasketListState = InitialState;
}

