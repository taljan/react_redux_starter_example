import {CartReducer} from "../../_store/CartReducer";
import {BasketListState} from "./BasketListState";
import {CartAction} from "../../../../action/CartAction";
import {BasketListStateStore} from "./BasketListReducer";

export const basketListReducer = new BasketListStateStore(BasketListStateStore.InitialState)
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: BasketListState, action: CartAction) => {
            return action.payload.entries;

        })
    .build();
