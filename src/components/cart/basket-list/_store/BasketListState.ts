import {CartEntryData} from "../../../../model/cart/CartEntryData";

export interface BasketListState extends Array<CartEntryData> {

}
