import * as React from "react";
import {Component} from "react";
import {ProductData} from "../../../../model/cart/ProductData";

interface Properties {
    product: ProductData,
    quantity: number,
}

interface Dispatchers {
    setCount: (e: any) => void,
}

type ComponentProperties = Properties & Dispatchers;

export class QuantityPanelComponent extends Component<ComponentProperties> {
    setCount = (e: any) => {
        this.props.setCount(e)
    };

    onQuantityChange = (e: any) => {
        this.setCount(e);
    };

    quantitySelectFields = () => {
        let options: Array<JSX.Element> = [];

        for (let i = 1; i < 100; i++) {
            options.push(<option value={i} key={i}>{i}</option>)
        }

        return <select value={this.props.quantity.toString()} onChange={this.onQuantityChange}>{options}</select>;
    };

    render() {
        return (
            <div style={{margin: "20px 0 0 0"}} className={"quantity-panel"}>
                Anzahl:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{this.quantitySelectFields()}
            </div>
        )
    }
}
