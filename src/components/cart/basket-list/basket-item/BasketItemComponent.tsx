import * as React from "react";
import {BasketItemData} from "../../../../model/cart/BasketItemData";
import {CurrencyFilter} from "../../../../filter/CurrencyFilter";
import {PriceFilter} from "../../../../filter/PriceFilter";
import {ProductImageFilter} from "../../../../filter/ProductImageFilter";
import {QuantityPanelComponent} from "./QuantityPanelComponent";
import {PriceInfoPanelComponent} from "./PriceInfoPanelComponent";

interface Properties {
    item: BasketItemData
}

type ComponentProperties = Properties;

export class BasketItemComponent extends React.Component<ComponentProperties> {
    constructor(props: ComponentProperties) {
        super(props);
        this.state = {
            productCount: props.item.quantity
        }
    }

    state: { productCount: number };

    setCount = (e: any): void => {
        //this.props.onSetCount(parseInt(this.props.item.product.productId), e.currentTarget.value);
    };

    removeProduct = (): void => {
        //this.props.onRemove(parseInt(this.props.item.product.productId));
    };

    productDetails = () => {
        return (
            <div>
                <br/>
                <h5>{this.props.item.product.masterProductName || this.props.item.product.designerCategoryName}<br/>
                    <small>{this.props.item.product.designerCategoryName}</small>
                </h5>
                <p>{this.props.item.product.classificationClassAsString.trim()}, {this.props.item.product.contentUnit}</p>
                <p>{CurrencyFilter(this.props.item.price.currency)} {PriceFilter(this.props.item.price.loweredPrice)}<br/>
                    <small>{CurrencyFilter(this.props.item.price.currency)} {PriceFilter(this.props.item.price.basePrice)} / {this.props.item.price.baseUnitSize}</small>
                </p>
            </div>
        );
    };

    render() {
        return (
            <div>
                <div className={"row"}>
                    <div className={"col-3"} style={{padding: "0 10px 0 10px"}}>
                        {ProductImageFilter(this.props.item.product.image, "80%")}
                    </div>

                    <div className={"col"}>
                        {this.productDetails()}
                    </div>

                    <div className={"col"}>
                        <QuantityPanelComponent
                            product={this.props.item.product}
                            quantity={this.props.item.quantity}
                            setCount={this.setCount}/>
                        <a href={"#"} onClick={(e) => {
                            e.preventDefault();
                            this.removeProduct()
                        }}>Löschen</a>
                    </div>

                    <div className={"col"}>
                        <PriceInfoPanelComponent price={this.props.item.price} quantity={this.props.item.quantity}/>
                    </div>

                </div>
                <div className={"row"}>
                    <div className={"col"}>
                        <hr/>
                    </div>
                </div>
            </div>
        );
    }
}
