import * as React from "react";
import {Component} from "react";
import {ProductPriceData} from "../../../../model/cart/ProductPriceData";
import {CurrencyFilter} from "../../../../filter/CurrencyFilter";
import {PriceFilter} from "../../../../filter/PriceFilter";


interface Properties {
    price: ProductPriceData,
    quantity: number
}

export class PriceInfoPanelComponent extends Component<Properties> {
    priceLowered = () => {
        return this.props.price.basePrice !== this.props.price.loweredPrice;
    };

    render() {
        return (
            <p className={"text-right float-right"} style={{margin: "20px 40px 0 0"}}>
                {`${CurrencyFilter(this.props.price.currency)} ${PriceFilter(this.props.price.loweredPrice * this.props.quantity)}`}
            </p>
        )
    }
}
