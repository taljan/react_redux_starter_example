import * as React from "react";
import {Component} from "react";
import {ApplicationStore} from "../../../store/Store";
import {connect} from "react-redux";
import {BasketListState} from "./_store/BasketListState";
import {CartEntryData} from "../../../model/cart/CartEntryData";
import {BasketItemComponent} from "./basket-item/BasketItemComponent";

interface Subscriptions {
    basketListState?: BasketListState
}

const mapStateToProps = (state: ApplicationStore) => {
    return {
        basketListState: state.basketListState
    }
};

export class BasketListComponent extends Component<Subscriptions> {
    render() {
        return (
            <div id={`basketList`}>
                {this.props.basketListState && this.props.basketListState.map((basketItem: CartEntryData) =>
                    <BasketItemComponent key={basketItem.product.productId} item={basketItem}/>)
                }
            </div>
        );
    }
}

export default connect<Subscriptions>(mapStateToProps)(BasketListComponent);
