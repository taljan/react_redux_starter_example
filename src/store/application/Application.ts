import {CustomerReducer} from "../../components/cart/customer/_store/CustomerReducer";
import {ApplicationState} from "./ApplicationState";
import {CartReducer} from "../../components/cart/_store/CartReducer";
import {ApplicationReducer} from "./ApplicationReducer";

export const applicationReducer = new ApplicationReducer(ApplicationReducer.InitialState)
    .subscribe(
        CustomerReducer.Actions.ReceiveCustomerData,
        (state: ApplicationState) => {
            return {
                ...state,
                hasCustomerData: true
            }
        }
    )
    .subscribe(
        CartReducer.Actions.ReceiveCartData,
        (state: ApplicationState) => {
            return {
                ...state,
                hasCart: true
            }
        }
    )
    .build();
