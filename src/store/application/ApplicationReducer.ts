import {AbstractStore} from "../Reducer";
import {ApplicationState} from "./ApplicationState";

const InitialState: ApplicationState = {
    hasAcceptedAgb: false,
    hasCustomerData: false,
    fetchingCart: false,
    hasCart: false,
};

export class ApplicationReducer extends AbstractStore<ApplicationState> {
    public static InitialState: ApplicationState = InitialState;
}

