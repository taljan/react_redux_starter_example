export interface ApplicationState {
    hasAcceptedAgb: boolean;
    hasCustomerData: boolean;
    fetchingCart: boolean;
    hasCart: boolean;
}
