import {applyMiddleware, createStore, Store} from "redux";
import {rootReducer} from "./Reducer";
import {Logger} from "../middleware/Logger";
import {ApplicationState} from "./application/ApplicationState";
import {CustomerState} from "../components/cart/customer/_store/CustomerState";
import {ErrorState} from "../components/error/_store/ErrorState";
import {BasketListState} from "../components/cart/basket-list/_store/BasketListState";
import {CartState} from "../components/cart/_store/CartState";
import {VoucherState} from "../components/cart/voucher/_store/VoucherState";
import {AsyncDispatch} from "../middleware/AsyncDispatch";
import {AddressState} from "../components/cart/address/_store/AddressState";
import {DeliveryOptionsListState} from "../components/cart/delivery-options/_store/DeliveryOptionsListState";
import {PaymentOptionsListState} from "../components/cart/payment-options/_store/PaymentOptionsListState";

let localState: any = JSON.parse(localStorage.getItem("dbt-checkout"));

if (localState === null) {
    localState = {};
}

export interface ApplicationStore extends Store {
    applicationState: ApplicationState,
    addressState: AddressState,
    customerState: CustomerState,
    errorState: ErrorState,
    cartDataState: CartState,
    basketListState: BasketListState,
    voucherState: VoucherState,
    deliveryOptionsState: DeliveryOptionsListState,
    paymentOptionsState: PaymentOptionsListState,
    employeeVoucherState: boolean,
    newCustomerVoucherState: boolean
}

export const store: Store = createStore(rootReducer, {...localState}, applyMiddleware(Logger, AsyncDispatch));
