import {Action, combineReducers} from "redux";
import {ApplicationStore} from "./Store";

export interface IActionMapper {
    type: string;
    callBack: (state: any, action: Action) => any;
}

export abstract class AbstractStore<T> {
    constructor(initialState: T) {
        this.initialState = initialState;
    }

    public Subscribers: Array<IActionMapper> = [];

    public subscribe(action: string, callBack: (state: T, action: Action) => T): AbstractStore<T> {
        this.Subscribers.push({type: action, callBack: callBack});
        return this;
    }

    private readonly initialState: T;

    build(): (state: T, action: Action) => T {
        return (state: T = this.initialState, action: Action): T => {
            this.Subscribers.map(mapper => {
                    if (mapper.type === action.type) {
                        state = mapper.callBack(state, action);
                        //TODO: delete if logger middleware is approved.
                        // console.log(this.constructor.name, state);
                    }
                }
            );
            return state;
        }
    }
}

import {applicationReducer} from "./application/Application";
import {addressReducer} from "../components/cart/address/_store/Address";
import {customerReducer} from "../components/cart/customer/_store/Customer";
import {errorReducer} from "../components/error/_store/Error";
import {cartReducer} from "../components/cart/_store/Cart"
import {basketListReducer} from "../components/cart/basket-list/_store/BasketList";
import {voucherReducer} from "../components/cart/voucher/_store/Voucher";
import {deliveryOptionsListReducer} from "../components/cart/delivery-options/_store/DeliveryOptionsList";
import {paymentOptionsListReducer} from "../components/cart/payment-options/_store/PaymentOptionsList";
import {employeeVoucherReducer} from "../components/cart/voucher/_store/employee/EmployeeVoucher";
import {newCustomerVoucherReducer} from "../components/cart/voucher/_store/new-customer/NewCustomerVoucher";

export const rootReducer = (state: ApplicationStore, action: Action) => {
    return combineReducers({
        applicationState: applicationReducer,
        addressState: addressReducer,
        customerState: customerReducer,
        errorState: errorReducer,
        cartDataState: cartReducer,
        basketListState: basketListReducer,
        voucherState: voucherReducer,
        deliveryOptionsState: deliveryOptionsListReducer,
        paymentOptionsState: paymentOptionsListReducer,
        employeeVoucherState: employeeVoucherReducer,
        newCustomerVoucherState: newCustomerVoucherReducer
    })(state, action);
};
