import {ApplicationStore} from "../store/Store";
import {BasicAction} from "../action/BasicAction";

export const Logger = (store: ApplicationStore) => (next: (action: BasicAction) => void) => (action: BasicAction) => {
    next(action);
    console.log(action);
    console.log(store.getState());
};
