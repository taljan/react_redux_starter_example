import {ApplicationStore} from "../store/Store";
import {BasicAction} from "../action/BasicAction";


export const AsyncDispatch = (store: ApplicationStore) => (next: (actionWithAsyncDispatch: any) => void) => (action: BasicAction) => {
    let syncActivityFinished: boolean = false;
    let actionQueue: Array<BasicAction> = [];

    let flushQueue = () => {
        actionQueue.forEach((action: BasicAction) => {
            store.dispatch(action);
        });
    };

    let asyncDispatch = (asyncAction: BasicAction) => {
        actionQueue = actionQueue.concat([asyncAction]);
        if (syncActivityFinished) {
            flushQueue();
        }
    };

    const actionWithAsyncDispatch =
        Object.assign({}, action, {asyncDispatch});

    next(actionWithAsyncDispatch);
    syncActivityFinished = true;
    flushQueue();
};
