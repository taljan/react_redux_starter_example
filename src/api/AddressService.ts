import {CustomerAddressData} from "../model/customer/CustomerAddressData";
import Axios from "axios";
import {API_ENDPOINT, APP_NAME} from "./Configuration";

export class AddressService {
    public static async checkAddress(address: CustomerAddressData) {
        return await Axios.post(`${API_ENDPOINT}/${APP_NAME}/address/v1/check.json`, address);

    }
}
