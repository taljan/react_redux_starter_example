import {KeyValuePair} from "../filter/KeyValuePair";

export class QueryService {
    public static getQueryData(): Array<KeyValuePair> {
        let queryParams: Array<KeyValuePair> = [];

        let search = window.location.search;

        let paramPairs: string[] = decodeURIComponent(search).trim().replace("?", "").split("&");
        paramPairs.forEach((paramPairStr: string) => {
            let paramPair: string[] = paramPairStr.split("=");
            queryParams.push({
                key: paramPair[0],
                value: paramPair[1]
            })
        });

        return queryParams;
    }

    public static getCartId(): string {
        return this.getQueryData().find((keyValuePair: KeyValuePair) => {
            return keyValuePair.key === `cartID`;
        }).value;
    }
}
