// TODO: Get from environment variable
import {AxiosError} from "axios";
import {store} from "../store/Store";
import {ErrorAction} from "../action/ErrorAction";
import {ErrorReducer} from "../components/error/_store/ErrorReducer";

export const API_ENDPOINT: string = "http://localhost:9001";

export const APP_NAME: string = "douglasBeautytab";

export const VOUCHER_EMPLOYEE: string = "PSNR23";
export const VOUCHER_NEW_CUSTOMER: string = "DCNEUK";

export const handleError = (error: AxiosError): void => {
    if (error.response) {
        store.dispatch(new ErrorAction(ErrorReducer.Actions.ReceiveError, error.response.data));
    } else {
        console.error(error);
    }
};
