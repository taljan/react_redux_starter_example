import Axios, {AxiosError, AxiosResponse} from "axios";
import {API_ENDPOINT, APP_NAME, handleError} from "./Configuration";
import {store} from "../store/Store";
import {CartAction} from "../action/CartAction";
import {PaymentOptionsListReducer} from "../components/cart/payment-options/_store/PaymentOptionsListReducer";
import {DeliveryOptionsListReducer} from "../components/cart/delivery-options/_store/DeliveryOptionsListReducer";
import {CustomerAddressData} from "../model/customer/CustomerAddressData";
import {QueryService} from "./QueryService";
import {CartReducer} from "../components/cart/_store/CartReducer";

export class CartService {
    public static fetchCart(cartId: string) {
        Axios.get(`${API_ENDPOINT}/${APP_NAME}/cart/${cartId}.json`).then((response: AxiosResponse) => {
            store.dispatch(new CartAction(CartReducer.Actions.ReceiveCartData, response.data));
        });
    }

    public static addVoucher(douglasVoucherCode: string) {
        Axios.get(`${API_ENDPOINT}/${APP_NAME}/voucher/${QueryService.getCartId()}/${douglasVoucherCode}.json`)
            .then((response: AxiosResponse) => {
                store.dispatch(new CartAction(CartReducer.Actions.ReceiveCartData, response.data));
            })
            .catch((error: AxiosError) => {
                handleError(error);
            });
    }

    public static fetchAvailablePaymentOptions(cartId: string) {
        Axios.get(`${API_ENDPOINT}/${APP_NAME}/payment/v1/${cartId}/availablePayments.json`)
            .then((response: AxiosResponse) => {
                store.dispatch(new CartAction(PaymentOptionsListReducer.Actions.ReceiveAvailablePaymentOptions, response.data));
            })
            .catch((error: AxiosError) => {
                handleError(error);
            });
    }

    public static fetchAvailableDeliveryOptions(cartId: string) {
        Axios.get(`${API_ENDPOINT}/${APP_NAME}/deliverymode/v1/${cartId}.json`)
            .then((response: AxiosResponse) => {
                store.dispatch(new CartAction(DeliveryOptionsListReducer.Actions.ReceiveAvailableDeliveryOptions, response.data));
            })
            .catch((error: AxiosError) => {
                handleError(error);
            });
    }

    public static async setDeliveryAddress(address: CustomerAddressData): Promise<void> {
        await Axios.post(`${API_ENDPOINT}/${APP_NAME}/address/v1/${QueryService.getCartId()}/delivery/addorupdate.json`, address);
    }

    public static async setBillingAddress(address: CustomerAddressData): Promise<void> {
        await Axios.post(`${API_ENDPOINT}/${APP_NAME}/address/v1/${QueryService.getCartId()}/billing/addorupdate.json`, address);
    }

    public static setDeliveryOption(deliveryOptionId: string) {
        Axios.post(`${API_ENDPOINT}/${APP_NAME}/deliverymode/v1/${QueryService.getCartId()}.json`, {
            selectedDeliveryMode: deliveryOptionId
        }).then((response: AxiosResponse) => {
            /**
             * TODO: Workaround until the backend sends back the cart in this request
             *
             */
            this.fetchCart(QueryService.getCartId());
        })
            .catch((error: AxiosError) => {
                handleError(error);
            });
    }

    public static setPaymentOption(paymentOptionId: string) {
        Axios.post(`${API_ENDPOINT}/${APP_NAME}/payment/v1/${QueryService.getCartId()}.json`, {
            paymentType: paymentOptionId
        }).then((response: AxiosResponse) => {
            /**
             * TODO: Workaround until the backend sends back the cart in this request
             */
            this.fetchCart(QueryService.getCartId());
        })
            .catch((error: AxiosError) => {
                handleError(error);
            });
    }
}
