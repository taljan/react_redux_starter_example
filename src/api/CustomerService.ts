import {CustomerData} from "../model/customer/CustomerData";
import Axios, {AxiosResponse} from "axios";
import {API_ENDPOINT, APP_NAME} from "./Configuration";
import {store} from "../store/Store";
import {CustomerAction} from "../action/CustomerAction";
import {CartReducer} from "../components/cart/_store/CartReducer";
import {Customer} from "../model/customer/Customer";
import {QueryService} from "./QueryService";
import {CartService} from "./CartService";


export class CustomerService {
    public static async createCustomer(): Promise<any> {

        let customerData: CustomerData = Customer.build(QueryService.getQueryData());
        customerData.walkInCustomer = !customerData.email;

        if (customerData.email) {
            let response: AxiosResponse = await Axios.post(`${API_ENDPOINT}/${APP_NAME}/customer/v1/${QueryService.getCartId()}.json`, customerData);
            store.dispatch(new CustomerAction(CartReducer.Actions.ReceiveCartData, response.data));
            return CartService.setDeliveryAddress(customerData.address);
        }
    }
}
