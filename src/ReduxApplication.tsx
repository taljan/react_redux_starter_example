import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {store} from "./store/Store";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Customer from "./components/cart/customer/CustomerComponent";
import BasketRoute from "./routes/BasketRoute";
import ErrorHandler from "./components/error/ErrorNotificationComponent";
import "./scss/App.scss";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div className={`container`}>
                <ErrorHandler/>

                <Customer/>
                <Switch>
                    <Route exact path={"/"} component={BasketRoute}/>
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById("app")
);
