export const CountryCodeFilter = (countryCode: string): string => {
    if (countryCode.toLowerCase() === "de") {
        return "Deutschland";
    } else {
        return "Europa"
    }
};
