export const PriceFilter = (price: number): string => {
    return price.toFixed(2).replace(/\./ig, ",");
};
