export const GenderFiler = (gender: string): string => {
    if (gender.toLowerCase() === "female" || gender.toLowerCase() === "f") {
        return "Frau";
    } else {
        return "Herr"
    }
};
