import * as React from "react";

export const ProductImageFilter = (source: string, width: string): JSX.Element => {
    return <img className={"product-image"} src={source} width={width}/>;
};
